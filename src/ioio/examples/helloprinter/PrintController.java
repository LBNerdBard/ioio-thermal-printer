package ioio.examples.helloprinter;

import java.util.concurrent.ArrayBlockingQueue;

import android.util.Log;

public class PrintController {

	ArrayBlockingQueue<Integer> out_queue_;
	private static String DEBUG_TAG = "Printer";
	
	public PrintController(ArrayBlockingQueue<Integer> out_queue_){
		this.out_queue_ = out_queue_;
	}
	
	public void setDefault() {
		wake();
		justify('L');
		inverseOff();
		doubleHeightOff();
		setLineHeight();
		boldOff();
		underlineOff();
		setBarcodeHeight(50);
		setSize('m');
	}

	public void toSleep() {
		Log.i(DEBUG_TAG,"sleeping");
		out_queue_.add(27);
		out_queue_.add(61);
		out_queue_.add(0);
	}

	public void printBuffer(){
		Log.i(DEBUG_TAG,"printing buffer");
		out_queue_.add(27);
		out_queue_.add(74);
		out_queue_.add(0);
	}
	
	
	public void setSize(char size) {
		Log.i(DEBUG_TAG,"setSize()");
		
		
		  int sizeval = 0;
		  
		  if(size == 's' || size == 'S') sizeval = 0;
		  if(size == 'm' || size == 'M') sizeval = 10;
		  if(size == 'l' || size == 'L') sizeval = 25;
		  
		  out_queue_.add(29);
		  out_queue_.add(33);
		  out_queue_.add(sizeval);
		  out_queue_.add(10);
	}
	
	public void printText(String out){
		
		for (int i = 0; i < out.length(); i++){
			int j = (int) out.charAt(i);
		    out_queue_.add(j);
		}
	}

	public void underlineOff() {
		Log.i(DEBUG_TAG,"underlineOff()");
		out_queue_.add(27);
		out_queue_.add(45);
		out_queue_.add(0);
		out_queue_.add(10);
	}

	public void underlineOn() {
		Log.i(DEBUG_TAG,"underlineOff()");
		out_queue_.add(27);
		out_queue_.add(45);
		out_queue_.add(1);
	}

	public void setBarcodeHeight(int height) {
		Log.i(DEBUG_TAG,"setBarcodeHeight()");
		out_queue_.add(29);
		out_queue_.add(104);
		out_queue_.add(height);
	}
	
	public void boldOn() {
		Log.i(DEBUG_TAG,"boldOff()");
		out_queue_.add(27);
		out_queue_.add(69);
		out_queue_.add(1);
	}
	
	public void feedline(){
		Log.i(DEBUG_TAG,"feedline()");
		out_queue_.add((int)'\n');
	}
	public void boldOff() {
		Log.i(DEBUG_TAG,"boldOff()");
		out_queue_.add(27);
		out_queue_.add(69);
		out_queue_.add(0);
		
	}

	public void setLineHeight(int height) {
		Log.i(DEBUG_TAG,"setLineHeight()");
		out_queue_.add(27);
		out_queue_.add(51);
	    out_queue_.add(height);
	}
	
	public void setLineHeight() {
		Log.i(DEBUG_TAG,"setLineHeight()");
		setLineHeight(32);
	}

	public void doubleHeightOff() {
		Log.i(DEBUG_TAG,"doubleHeightOff()");
		out_queue_.add(27);
		out_queue_.add(20);
	}

	public void doubleHeightOn() {
		Log.i(DEBUG_TAG,"doubleHeightOn()");
		out_queue_.add(27);
		out_queue_.add(14);
	}
	
	public void inverseOff() {
		Log.i(DEBUG_TAG,"inverseOff()");
		out_queue_.add(29);
		out_queue_.add(66);
		out_queue_.add(0);
		out_queue_.add(10);
	}
	
	public void inverseOn() {
		Log.i(DEBUG_TAG,"inverseOff()");
		out_queue_.add(29);
		out_queue_.add(66);
		out_queue_.add(1);
	}

	public void justify(char arg) {
		Log.i(DEBUG_TAG,"justify()");
		
		int position = 0;
		
		if(arg == 'l' || arg == 'L') 
			position = 0;
		if(arg == 'c' || arg == 'C') 
			position = 1;
		if(arg == 'r' || arg == 'R') 
			position = 2;  
		
		
		out_queue_.add(27);
		out_queue_.add(97);
		out_queue_.add(position);
		
		
	}

	public void wake() {
		Log.i(DEBUG_TAG,"waking");
		out_queue_.add(27);
		out_queue_.add(61);
		out_queue_.add(1);
	}

	public void modifyPrintDensity() {
		Log.i(DEBUG_TAG,"modifying print density and timeout");
		out_queue_.add(18);
		out_queue_.add(35);
		out_queue_.add(255);
	}

	public void initPrinterSettings() {
		Log.i(DEBUG_TAG,"Setting variables");
		out_queue_.add(27);
		out_queue_.add(55);
		out_queue_.add(16);
		out_queue_.add(240);
		out_queue_.add(2);
	}
}
