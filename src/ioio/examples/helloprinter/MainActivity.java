package ioio.examples.helloprinter;

import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.ArrayBlockingQueue;

import ioio.examples.helloprinter.R;
import ioio.lib.api.IOIO;
import ioio.lib.api.Uart;
import ioio.lib.api.Uart.Parity;
import ioio.lib.api.Uart.StopBits;
import ioio.lib.api.exception.ConnectionLostException;
import ioio.lib.util.BaseIOIOLooper;
import ioio.lib.util.IOIOLooper;
import ioio.lib.util.android.IOIOActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

/**
 * This is the main activity of the HelloIOIO example application.
 * 
 * It displays a toggle button on the screen, which enables control of the
 * on-board LED. This example shows a very simple usage of the IOIO, by using
 * the {@link IOIOActivity} class. For a more advanced use case, see the
 * HelloIOIOPower example.
 */
public class MainActivity extends IOIOActivity {

	private Button print_;
	private Button linefeed_;
	private EditText inputField_;
	private ArrayBlockingQueue<Integer> out_queue_ = new ArrayBlockingQueue<Integer>(1024);
	private PrintController printerControl;
	/**
	 * Called when the activity is first created. Here we normally initialize
	 * our GUI.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		print_ = (Button) findViewById(R.id.print);
		linefeed_ = (Button) findViewById(R.id.linefeed);
		
		inputField_ = (EditText) findViewById(R.id.editText1);
		printerControl = new PrintController(out_queue_);
		print_.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				printerControl.wake();

				String out = inputField_.getText().toString();
				if (out.length()>0)
					printerControl.printText(out);
				
				printerControl.printBuffer();
				printerControl.toSleep();
			}
			
		});
		
		linefeed_.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				printerControl.wake();
				printerControl.feedline();
				printerControl.toSleep();
			}
			
		});
	}
	
	

	/**
	 * This is the thread on which all the IOIO activity happens. It will be run
	 * every time the application is resumed and aborted when it is paused. The
	 * method setup() will be called right after a connection with the IOIO has
	 * been established (which might happen several times!). Then, loop() will
	 * be called repetitively until the IOIO gets disconnected.
	 */
	class Looper extends BaseIOIOLooper {

		private Uart printer;
		OutputStream printerout;
		
		/**
		 * Called every time a connection with IOIO has been established.
		 * Typically used to open pins.
		 * 
		 * @throws ConnectionLostException
		 *             When IOIO connection is lost.
		 * 
		 * @see ioio.lib.util.AbstractIOIOActivity.IOIOThread#setup()
		 */
		@Override
		protected void setup() throws ConnectionLostException {
			printer = ioio_.openUart(IOIO.INVALID_PIN, 7, 19200, Parity.NONE, StopBits.ONE);
			printerout = printer.getOutputStream();
			printerControl.initPrinterSettings();
			printerControl.modifyPrintDensity();
			printerControl.setDefault();
			printerControl.toSleep();			
		}

		

		/**
		 * Called repetitively while the IOIO is connected.
		 * 
		 * @throws ConnectionLostException
		 *             When IOIO connection is lost.
		 * 
		 * @see ioio.lib.util.AbstractIOIOActivity.IOIOThread#loop()
		 */
		@Override
		public void loop() throws ConnectionLostException {
			try {
				Integer i = out_queue_.poll();
				if (i!=null){
					if (printerout != null){
					    printerout.write(i);
						printerout.flush();
					}
					
					Thread.sleep(100);
				}
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
			}
		}
	}

	/**
	 * A method to create our IOIO thread.
	 * 
	 * @see ioio.lib.util.AbstractIOIOActivity#createIOIOThread()
	 */
	@Override
	protected IOIOLooper createIOIOLooper() {
		return new Looper();
	}
}